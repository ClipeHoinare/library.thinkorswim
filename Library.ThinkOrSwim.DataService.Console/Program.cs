﻿using AsyncWindowsClipboard;
using Library.Core.Repository.Screenshot;
using Library.Repository.StorageArmory;
using Library.ThinkOrSwim.DataService.Importer;
using Library.ThinkOrSwim.DataService.Processor;
using System;
using System.Diagnostics;
using static Library.Core.Repository.Screenshot.NativeMethods;

namespace Library.ThinkOrSwim.DataService.Console
{
    class Program
    {
        static ToSSikuliController tosController = new ToSSikuliController();

        static void Main(string[] args)
        {
            //var clipboardService = new WindowsClipboardService(timeout: TimeSpan.FromMilliseconds(200));
            //await clipboardService.SetTextAsync("Hello world"); // Sets the text
            //ar data = await clipboardService.GetTextAsync(); // Reads "Hello world"

            //PlayJanuary();



            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-04 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-05 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-11 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-12 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-18 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-19 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-20 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-25 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-26 09:30:00"));

            //tosController.SetToSDateAndTime(DateTime.Parse("2020-03-17 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-04-01 09:30:00"));


            //tosController.SetToSDateAndTime(DateTime.Parse("2019-11-15 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-17 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-02-21 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-03-20 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-06-19 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-09-18 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-10-16 09:30:00"));

            //tosController.SetToSDateAndTime(DateTime.Parse("2020-11-15 09:30:00"));



            //ChromeWrapper wrapper = new ChromeWrapper("");
            //wrapper.SendKey((char)3);

            (new StorageSqlQueue()).Start();
            var c1 = new TosMarketStocksDataProcessor();

            c1.Process(null);

            (new StorageSqlQueue()).Stop();
        }

        private static void PlayJanuary()
        {
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-02 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-03 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-06 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-07 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-08 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-09 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-10 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-13 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-14 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-15 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-16 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-17 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-21 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-22 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-23 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-24 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-27 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-28 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-29 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-30 09:30:00"));
            //tosController.SetToSDateAndTime(DateTime.Parse("2020-01-31 09:30:00"));
        }
    }
    }
