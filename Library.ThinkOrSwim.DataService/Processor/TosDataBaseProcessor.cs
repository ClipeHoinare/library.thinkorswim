﻿using Library.Core.Trade;
using Library.Domain.Infrastructure;
using Library.Domain.Jackdow;
using Library.Domain.Trade.Base;
using Library.Repository.Base;
using Library.Repository.LogRepository;
using Library.Repository.StorageArmory;
using Library.ThinkOrSwim.DataService.Importer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Library.ThinkOrSwim.DataService.Processor
{
    public abstract class TosDataBaseProcessor<T> : PilingBaseProcessor
    {
        #region variables
        protected IImporterInter<QuoteRealTimeItem, FinanceParameter> _tosFinanceImporter = null;
        protected ToSSikuliController _tosSikuliController = null;

        protected ClassFactory _optionMetricsFactory = new ClassFactory(ClassFactory.DenKey.OptionMetrics);

        protected string _crlf = "\n\r";

        public TosDataBaseProcessor()
        {
        }
        #endregion
        #region public area
        public bool Process(string stringParameters = null)
        {
            bool succesfully = true;

            DateTime fromDate;
            DateTime toDate;
            fromDate = DateTime.Parse("2020-01-22").AddDays(-1);
            toDate = DateTime.Parse("2020-01-31");

            var tradingDateTime = fromDate;
            while (fromDate < toDate)
            {
                fromDate = GetNextDateToProcess(fromDate);
                if (fromDate > toDate)
                    break;

                tradingDateTime = fromDate.AddHours(11).AddMinutes(30);
                tradingDateTime = tradingDateTime.AddMinutes(-30);

                for (int cnt = 0; cnt < 16; cnt++)
                {
                    tradingDateTime = tradingDateTime.AddMinutes(30);
                    if (tradingDateTime.Hour > 16 || tradingDateTime.Hour == 16 && tradingDateTime.Minute >= 30)
                        break;

                    // cnt==0 signal first change of the date, sequence are changing only the hour
                    if (_tosSikuliController.SetToSDateAndTime(tradingDateTime, !(cnt == 0)))
                    {
                        DoImporting(tradingDateTime);
                    }
                    //break;
                }
            }

            return succesfully;
        }
        #endregion
        #region protected area
        protected void DoImporting(DateTime tradingDateTime)
        {
            Console.WriteLine("========================================================================={0}", tradingDateTime);
            var found = false;
            //Get a list of stocks with their flags
            var stockList = GetStockIdsForPull();

            //loop thru the stock list and start the process sending all requests
            for (int cntStock = 0; cntStock < stockList.Count; cntStock++)
            {
                var secid = Convert.ToInt32(stockList[cntStock]["secid"]);
                var symbol = Convert.ToString(stockList[cntStock]["ticker"]);
                var secType = Convert.ToString(stockList[cntStock]["secType"]);

                var parameter = new FinanceParameter() { SecId = secid, Symbol = symbol, SecType = secType, StartDateTime = tradingDateTime };

                _tosFinanceImporter.AddParameter(parameter);

                var tuple = _tosFinanceImporter.GetOneToProcess();

                if (tuple != null && tuple.Item1 != null && tuple.Item2 is null)
                {
                    BeforeImporting(tuple.Item1);
                    SaveStockValuesBySQL(tuple.Item1);
                    found = true;
                    AfterImporting(tuple.Item1, (found ? 1 : 0));
                }
                else
                {
                    found = false;
                    if (tuple.Item1 != null && tuple.Item2 != null)
                        ErrorLogger.LogException(tuple.Item2, ErrorLevel.Error, ErrorLoggerName.TosBox, String.Format("TosFinanceBaseProcessor.GetIndividualStockValues, stockID={0}", tuple.Item1.SecId));
                }
            }

            AfterProcessing(null);
        }
        protected void DoImporting2(DateTime tradingDateTime)
        {
            Console.WriteLine("========================================================================={0}", tradingDateTime);

            //Get a list of stocks with their flags
            var stockList = GetStockIdsForPull();

            //loop thru the stock list and start the process sending all requests
            for (int cntStock = 0; cntStock < stockList.Count; cntStock++)
            {
                var secid = Convert.ToInt32(stockList[cntStock]["secid"]);
                var symbol = Convert.ToString(stockList[cntStock]["ticker"]);
                var secType = Convert.ToString(stockList[cntStock]["secType"]);

                var parameter = new FinanceParameter() { SecId = secid, Symbol = symbol, SecType = secType, StartDateTime=tradingDateTime };

                _tosFinanceImporter.AddParameter(parameter);
            }

            // while waiting for everything to finish, process them as they are coming back
            var found = true;
            while (found)
            {
                var tuple = _tosFinanceImporter.GetOneToProcess();

                if (tuple != null && tuple.Item1 != null && tuple.Item2 is null)
                {
                    BeforeImporting(tuple.Item1);
                    SaveStockValuesBySQL(tuple.Item1);
                    found = true;
                    AfterImporting(tuple.Item1, (found ? 1 : 0));
                }
                else
                {
                    found = false;
                    if (tuple.Item2 != null)
                        ErrorLogger.LogException(tuple.Item2, ErrorLevel.Error, ErrorLoggerName.TosBox, String.Format("TosFinanceBaseProcessor.GetIndividualStockValues, stockID={0}", tuple.Item1.SecId));
                }
            }

            //after the loop of receiving the finished data we look again to see if we missed anything
            var leftData = _tosFinanceImporter.GetDataLeft();
            if (leftData != null && leftData.Item1 != null && leftData.Item1.Count > 0 && leftData.Item2 is null)
            {
                for (int i = 0; i < leftData.Item1.Count; i++)
                {
                    var realTimeQuote = leftData.Item1[i];

                    BeforeImporting(realTimeQuote);
                    SaveStockValuesBySQL(realTimeQuote);
                    AfterImporting(realTimeQuote, 1);
                }
            }
            else
            {
                if (leftData.Item2 != null)
                    ErrorLogger.LogException(leftData.Item2, ErrorLevel.Error, ErrorLoggerName.TosBox, String.Format("TosFinanceBaseProcessor.DoImporting III"));
            }

            AfterProcessing(null);
        }
        protected void GetIndividualStockValues(FinanceParameter parameter)
        {
            bool found = false;

            //TODO: see if the todos of the ImportSync were done
            var tuple = _tosFinanceImporter.ImportSync(parameter);
            if (tuple != null && tuple.Item2 is null && tuple.Item1 != null && tuple.Item1.Count > 0)
            {
                BeforeImporting(tuple.Item1);
                SaveStockValuesBySQL(tuple.Item1);
                found = true;
            }
            else
            {
                if (tuple.Item2 != null)
                    ErrorLogger.LogException(tuple.Item2, ErrorLevel.Error, ErrorLoggerName.TosBox, String.Format("TosFinanceBaseProcessor.GetIndividualStockValues, stockID={0}", parameter.SecId));
            }

            AfterImporting(tuple.Item1, new Tuple<int,int> (parameter.SecId, (found ? 1 : 0)));
        }
        protected void SaveStockValuesBySQL(dynamic input)
        {
            if (input != null)
            {
                var item = input as QuoteRealTimeItem;

                item.TradingDate = GetDaysForSAS(item.TradingDateTime);
                item.TradingTime = GetSecsForSAS(item.TradingDateTime);

                var data = GetCommandAndParameterForSave(item);

                SetLastSqlString(data.Item1, data.Item2);

                StorageSqlQueue.RingQueue.AddItem((new StorageSqlQueue.SqlBundle(ClassFactory.DenKey.OptionMetrics) { CommandText = data.Item1, Parameters = data.Item2 }));
            }
        }
        #endregion
        #region protected abstract area
        protected abstract Tuple<string, List<SqlParameter>> GetCommandAndParameterForSave(dynamic item);
        protected abstract List<Dictionary<string, object>> GetStockIdsForPull();
        protected abstract DateTime GetNextDateToProcess(DateTime fromDate);
        #endregion
    }
}
