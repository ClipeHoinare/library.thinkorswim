﻿using Library.Domain.Jackdow;
using Library.Domain.Trade.Base;
using Library.Repository.Base;
using Library.Repository.StorageArmory;
using Library.ThinkOrSwim.DataService.Importer;
using Library.Wrapper.Trady.Yahoo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Library.ThinkOrSwim.DataService.Processor
{
    public class TosMarketStocksDataProcessor : TosDataBaseProcessor<QuoteRealTimeItem>
    {
        public TosMarketStocksDataProcessor()
        {
            _tosFinanceImporter = new TosMarketImporter();
            _tosSikuliController = new ToSSikuliController();
        }
        protected override List<Dictionary<string, object>> GetStockIdsForPull()
        {
            string sqlCommand = @"
                select distinct sec.secid, sec.ticker,
					case when sec.index_flag = 1 and hlp.typeFlag = 1 then 'IDX' else 'STK' end secType
	                from OptionMetrics.dbo.[hlpSECURD] hlp 
	                inner join [OptionMetricsRaw].[dbo].[SECURD] sec on sec.secid=hlp.secid
                    order by sec.ticker
 ";

            SetLastSqlString(sqlCommand);

            var ds = _optionMetricsFactory.GetDataSet(sqlCommand);

            return _optionMetricsFactory.ConvertToList(ds);
        }
        protected override DateTime GetNextDateToProcess(DateTime dt)
        {
            //TODO: do the code here
            //throw new NotImplementedException();

            var sqlCommand = @"
                --declare @lastDate datetime;
                --set @lastDate = '2019-12-31';

                declare @nextDate datetime=@lastDate;
                declare @found int = 0;

                WHILE  @found = 0
                begin
	                set @found = 1;
	                set @nextDate = DATEADD(day, 1, @nextDate);

	                if DATEPART(dw,@nextDate) in (1,7)
	                begin
		                set @found = 0;
		                continue;
	                end

	                if exists(select * from OptionMetrics.dbo.Calendar cal where cal.[type]='holiday' and cal.date=@nextDate)
	                begin
		                set @found = 0;
		                continue;
	                end
                end

                select @nextDate;
 ";

            SetLastSqlString(sqlCommand);

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("lastDate", dt));

            var scalar = _optionMetricsFactory.ExecuteScalar(sqlCommand, parameters);

            return Convert.ToDateTime(scalar);
        }
        protected override Tuple<string, List<SqlParameter>> GetCommandAndParameterForSave(dynamic input)
        {
            string sqlCommand = @" 
                if not exists (select * from secprd where secid=@secid and date=@Date and time=@time and source=@source)
begin
	                INSERT INTO secprd (secid, date, time, source, ask, bid, [open], [close], high, low, last, mark, volume, cfadj, cfret) values (@secid, @date, @time, @source, @ask , @bid ,@open, @close, @high, @low, @last, @mark, @volume, 1, 1);
end
                else
begin
	                UPDATE secprd set ask=@ask, bid=@bid, [open]=@Open, [close]=@Close, high=@High , low=@Low , last=@last, mark=@mark, volume=@volume where secid=@secid and date=@Date and time=@time and source=@source;
end
";

            List<SqlParameter> parameters = new List<SqlParameter>();

            var item = input as QuoteRealTimeItem;

            parameters.Add(new SqlParameter("secid", item.SecId));
            parameters.Add(new SqlParameter("Date", item.TradingDate));
            parameters.Add(new SqlParameter("Time", item.TradingTime));
            parameters.Add(new SqlParameter("Ask", item.Ask));
            parameters.Add(new SqlParameter("Bid", item.Bid));
            parameters.Add(new SqlParameter("Open", item.Open));
            parameters.Add(new SqlParameter("Close", item.Close));
            parameters.Add(new SqlParameter("High", item.High));
            parameters.Add(new SqlParameter("Low", item.Low));
            parameters.Add(new SqlParameter("Last", item.Last));
            parameters.Add(new SqlParameter("Mark", item.Mark));
            parameters.Add(new SqlParameter("Volume", item.Volume));
            parameters.Add(new SqlParameter("source", 3));

            return new Tuple<string, List<SqlParameter>>(sqlCommand, parameters);
        }
        protected override void BeforeImporting(dynamic input)
        {
            var item = input as QuoteRealTimeItem;

           // item.TradingDate = 

        }
        protected override void AfterImporting(dynamic input, dynamic flags)
        {
            var item = input as QuoteRealTimeItem;

            var flag = (int)flags;

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("stockID", item.SecId));
            parameters.Add(new SqlParameter("enabledFlag", flag));

            string sqlCommand =
                " update st " +
                " set st.[Enabled] = @enabledFlag " +
                " from stock st " +
                " where st.StockID = @stockID ";

            SetLastSqlString(sqlCommand, parameters);
        }
    }
}
