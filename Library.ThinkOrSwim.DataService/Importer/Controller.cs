﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Library.ThinkOrSwim.Adapter
//{
//    public class Controller : IDisposable
//    {
//        public delegate void QuoteReceivedEventHandler(Controller sender, Quote quote, EventArgs e);
//        public event QuoteReceivedEventHandler QuoteReceived;
//        public delegate void StockReceivedEventHandler(Controller sender, Quote quote, EventArgs e);
//        public event StockReceivedEventHandler StockReceived;

//        private int _timeout;

//        private Client _client;
//        public int Timeout { get => _timeout; set => _timeout = value; }
//        public Client Client { get => _client; set => _client = value; }

//        public Controller(int timeout)
//        {
//            _timeout = timeout;
//            ActivateClient();
//        }
//        public void Dispose()
//        {
//            _client.Dispose();
//        }
//        public void AddStockRequest(string symbol, string stockType)
//        {
//            if (stockType.Equals("ETF") || stockType.Equals("STK"))
//            {
//                _client.Add(symbol, stockType, DataType.Ask);
//                _client.Add(symbol, stockType, DataType.Bid);
//                _client.Add(symbol, stockType, DataType.Close);
//                _client.Add(symbol, stockType, DataType.High);
//                _client.Add(symbol, stockType, DataType.Last);
//                _client.Add(symbol, stockType, DataType.Low);
//                _client.Add(symbol, stockType, DataType.Mark);
//                _client.Add(symbol, stockType, DataType.Open);
//            }
//            else if (stockType.Equals("IDX"))
//            {
//                _client.Add(symbol, stockType, DataType.Close);
//                _client.Add(symbol, stockType, DataType.High);
//                _client.Add(symbol, stockType, DataType.Last);
//                _client.Add(symbol, stockType, DataType.Low);
//                _client.Add(symbol, stockType, DataType.Mark);
//                _client.Add(symbol, stockType, DataType.Open);
//            }
//            else if (stockType.Equals("OPT"))
//            {
//                _client.Add(symbol, stockType, DataType.Ask);
//                _client.Add(symbol, stockType, DataType.Bid);
//                _client.Add(symbol, stockType, DataType.Close);
//                _client.Add(symbol, stockType, DataType.Delta);
//                _client.Add(symbol, stockType, DataType.Gamma);
//                _client.Add(symbol, stockType, DataType.High);
//                _client.Add(symbol, stockType, DataType.Last);
//                _client.Add(symbol, stockType, DataType.Low);
//                _client.Add(symbol, stockType, DataType.Mark);
//                _client.Add(symbol, stockType, DataType.Impl_vol);
//                _client.Add(symbol, stockType, DataType.Open);
//                _client.Add(symbol, stockType, DataType.Strike);
//                _client.Add(symbol, stockType, DataType.Theta);
//                _client.Add(symbol, stockType, DataType.Vega);
//                _client.Add(symbol, stockType, DataType.Expiration_day);
//            }
//            else
//            {
//                throw new Exception("not implemented for stockType {stockType}");
//            }

//        }

//        public void ActivateClient()
//        {
//            if(_client == null)
//            {
//                _client = new Client(_timeout);
//            }

//            if ( !_client.IsClientActive())
//            {
//                _client.Dispose();
//                _client = new Client(_timeout);
//            }
//        }

//        public Topics PullData()
//        {
//            bool found = true;
//            Quote quote;
//            while (found)
//            {
//                found = _client.GetOneQuote(out quote);

//                if (found && quote != null)
//                {
//                    QuoteReceived?.Invoke(this, quote, null);

//                    if (this._client.Topics.CheckIfSymbolCompleted(quote))
//                    {
//                        StockReceived?.Invoke(this, quote, null);

//                        this._client.Topics.SymbolComplete(quote);
//                    }

//                    //remove the request for the current quote type
//                    _client.Remove(quote.Symbol, (DataType)Enum.Parse(typeof(DataType), quote.DataType), quote.CounterId);
//                }

//                if (_client.CurrentCount<=0)
//                    break;
//            }

//            return _client.Topics;
//        }
//    }
//}
