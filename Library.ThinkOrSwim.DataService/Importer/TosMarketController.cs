﻿using Library.ThinkOrSwim.DataService.Importer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Library.ThinkOrSwim.Adapter
{
    public class TosMarketController : IDisposable
    {
        public delegate void NoiseReceivedEventHandler(TosMarketController sender, Tuple<int, double> tuple, EventArgs e);
        public event NoiseReceivedEventHandler NoiseReceived;
        public delegate void QuoteReceivedEventHandler(TosMarketController sender, Quote quote, EventArgs e);
        public event QuoteReceivedEventHandler QuoteReceived;
        public delegate void StockReceivedEventHandler(TosMarketController sender, Quote quote, EventArgs e);
        public event StockReceivedEventHandler StockReceived;

        private int _timeout;
        private Topics _topics;

        private Client _client;
        public int Timeout { get => _timeout; set => _timeout = value; }
        public Client Client { get => _client; set => _client = value; }
        public Topics Topics { get => _topics; }

        public TosMarketController(int timeout)
        {
            _timeout = timeout;
            _topics = new Topics();
            ActivateClient();
        }
        public void Dispose()
        {
            _client.Dispose();
        }
        public void AddStockRequest(string symbol, int secId, string stockType, DateTime dt)
        {
            if (stockType.Equals("ETF") || stockType.Equals("STK"))
            {
                AddElementRequest(symbol, secId, dt, stockType, DataType.Ask);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Bid);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Close);
                AddElementRequest(symbol, secId, dt, stockType, DataType.High);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Last);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Low);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Mark);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Open);
            }
            else if (stockType.Equals("IDX"))
            {
                AddElementRequest(symbol, secId, dt, stockType, DataType.Close);
                AddElementRequest(symbol, secId, dt, stockType, DataType.High);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Last);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Low);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Mark);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Open);
            }
            else if (stockType.Equals("OPT"))
            {
                AddElementRequest(symbol, secId, dt, stockType, DataType.Ask);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Bid);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Close);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Delta);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Gamma);
                AddElementRequest(symbol, secId, dt, stockType, DataType.High);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Last);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Low);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Mark);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Impl_vol);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Open);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Strike);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Theta);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Vega);
                AddElementRequest(symbol, secId, dt, stockType, DataType.Expiration_day);
            }
            else 
            {
                throw new Exception("not implemented for stockType {stockType}");
            }
        }
        protected void AddElementRequest(string symbol, int secId, DateTime dt, string stockType, DataType quoteType)
        {
            var type = quoteType.ToString();
            var counterId = (int)quoteType;
            var id = getHash(symbol, type, counterId);

            var quote = new Quote(id, symbol, type, counterId);

            _client.Add(quote);

            Topics.Add(quote, secId, dt, stockType);
        }
        int getHash(string symbol, string type, int counterId)
        {
            var value = string.Format("{0}:{1}:{2}", symbol, type, counterId);
            using (var h = MD5.Create())
            {
                return Math.Abs(BitConverter.ToInt16(
                    h.ComputeHash(Encoding.UTF8.GetBytes(value)), 0));
            }
        }
        public void ActivateClient()
        {
            if(_client == null)
            {
                _client = new Client(_timeout) { Quotes= new Quotes() };
            }

            if ( !_client.IsClientActive())
            {
                _client.Dispose();
                _client = new Client(_timeout) { Quotes = new Quotes() };
            }
        }
        public Topics PullData()
        {
            bool found = true;
            Quote quote;
            Tuple<int, double> tuple;
            while (found)
            {
                found = _client.GetOneQuote(out quote, out tuple);

                if (found && quote != null)
                {
                    Topics.Update(quote);

                    QuoteReceived?.Invoke(this, quote, null);

                    if (this.Topics.CheckIfSymbolCompleted(quote))
                    {
                        StockReceived?.Invoke(this, quote, null);

                        this.Topics.SymbolComplete(quote);
                    }

                    //remove the request for the current quote type
                    _client.Remove(quote.Id);
                }
                else if (found && tuple != null)
                {
                    NoiseReceived?.Invoke(this, tuple, null);
                }

                if (_client.CurrentCount<=0)
                    break;
            }

            return Topics;
        }
        public QuoteRealTime PullOneData()
        {
            var found = true;
            Quote quote = null;
            Tuple<int, double> tuple;

            QuoteRealTime quoteRealTime = null;

            while (found)
            {
                found = _client.GetOneQuote(out quote, out tuple);

                if (found && quote != null)
                {
                    Topics.Update(quote);

                    QuoteReceived?.Invoke(this, quote, null);

                    if (this.Topics.CheckIfSymbolCompleted(quote))
                    {
                        StockReceived?.Invoke(this, quote, null);

                        this.Topics.SymbolComplete(quote);
                    }

                    //remove the request for the current quote type
                    _client.Remove(quote.Id);
                }
                else if(found && tuple != null)
                {
                    NoiseReceived?.Invoke(this, tuple, null);
                }



                if (_client.CurrentCount <= 0 || this.Topics.CheckIfSymbolCompleted(quote))
                    break;
            }

            if(found && quote != null)
            {
                quoteRealTime = PullOneQuote(quote.Symbol);
            }

            //TODO: Tell client to remove all the requests that are still standing

            return quoteRealTime;
        }
        public Topics GetDataLeft()
        {
            bool found = true;
            Quote quote;
            Tuple<int, double> tuple;
            if (_client.CurrentCount <= 0)
                found = false;

            while (found)
            {
                found = _client.GetOneQuote(out quote, out tuple);

                if (found && quote != null)
                {
                    Topics.Update(quote);

                    QuoteReceived?.Invoke(this, quote, null);

                    if (this.Topics.CheckIfSymbolCompleted(quote))
                    {
                        StockReceived?.Invoke(this, quote, null);

                        this.Topics.SymbolComplete(quote);
                    }

                    //remove the request for the current quote type
                    _client.Remove(quote.Id);
                }
                else if (found && tuple != null)
                {
                    NoiseReceived?.Invoke(this, tuple, null);
                }

                if (_client.CurrentCount <= 0)
                    break;
            }

            return Topics;
        }
        public QuoteRealTime PullOneQuote(string symbol)
        {
            QuoteRealTime quoteRealTime = null;

            Topics.TryRemove(symbol, out quoteRealTime);

            return quoteRealTime;
        }
        public QuoteHeader GetOneHeader(string symbol)
        {
            QuoteHeader quoteHeader = Topics.QuoteHeaders[symbol];

            return quoteHeader;
        }
    }
}
