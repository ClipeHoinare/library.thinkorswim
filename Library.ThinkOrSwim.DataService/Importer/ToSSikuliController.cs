﻿using Library.Core.Repository.Screenshot;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Library.ThinkOrSwim.DataService.Importer
{
    public class ToSSikuliController : IDisposable
    {
        string _pathToImages = "C:\\projects\\sikuli\\ToSOnDemand\\";
        bool _firstRunFlag = true;
        ScreenshotManager _screenshotManager = new ScreenshotManager();

        /// <summary>
        /// Wait for the screen to change
        /// </summary>
        public int WaitTimeout { get; set; }

        public ToSSikuliController()
        {
            WaitTimeout = 45;
        }
        public void Dispose()
        {
        }
        public bool SetToSDateAndTime(DateTime dt, bool sameDate=false, bool expDate=false)
        {
            var succesful = true;

            //var day = dt.Day.ToString();
            var time = dt.ToString("HH:mm:ss");
            var flagForDaySet = sameDate && !expDate ? "S" : !sameDate && !expDate ? "D" : !sameDate && expDate ? "H" : "G";
            var flagForDayCheck = expDate ? "G" : "S";

            //Step 1: setup windows location and size
            var tosSize = _screenshotManager.SetLocationAndSize();
            _screenshotManager.SetNewDatetime();

            //Step 2:  check if ondemand & realtime data is turned on
            if (!_screenshotManager.CheckIfOnDemandIsOn())
            {
                Console.WriteLine("Exit after real time data on");
                succesful = false;
                return succesful;
            }

            //Step 4: clickon the calendar icon
            if (!_screenshotManager.ClickOnCalendar())
            {
                Console.WriteLine("Exit at the click on calendar");
                succesful = false;
                return succesful;
            }

            //Step 5: validate that the month and the year are the ones in the application as well
            if (!_screenshotManager.ValidateTheMonth(dt))
            {
                Console.WriteLine("Exit at validation of the month");
                succesful = false;
                return succesful;
            }

            //Step 6: select the day
            if (!_screenshotManager.SelectDay(dt, flagForDaySet))
            {
                Console.WriteLine("Exit at the click on the day");
                succesful = false;
                return succesful;
            }

            //Step 5: tripple-click to select the current time, then type the time to set
            if (!_screenshotManager.ClickOnAndSelectTime())
            {
                Console.WriteLine("Exit before setting time");
                succesful = false;
                return succesful;
            }

            //_session.Type(time);
                _screenshotManager.TypeTimeValue(time);

            //Step 9: commit the change
            if (!_screenshotManager.CommitTheChange())
            {
                Console.WriteLine("Exit at commiting the chenge");
                succesful = false;
                return succesful;
            }

            //Step 10: Wait to see if the calendar date changed, just the date
            if (!_screenshotManager.WaitForDateToChange(!sameDate && !_firstRunFlag))
            {
                succesful = false;
                _firstRunFlag = false;
                return succesful;
            }
            _firstRunFlag = false;

            //Step 11: clickon the calendar icon
            if (!_screenshotManager.ClickOnCalendar())
            {
                Console.WriteLine("Exit at the click second time on calendar");
                succesful = false;
                return succesful;
            }

            //Step 12: validate that the month and the year are the ones in the application as well
            if (!_screenshotManager.ValidateTheMonth(dt))
            {
                Console.WriteLine("Exit at second validation of the month");
                succesful = false;
                return succesful;
            }

            //Step 13: check selected day
            if (!_screenshotManager.CheckSelectedDay(dt, flagForDayCheck))
            {
                Console.WriteLine("Exit at the check of the day");
                succesful = false;
                return succesful;
            }

            //Step 14: No more actions done on the page, now validate that we did them
            //copy the time typed back to validate what was entered at step 4.2
            if (!_screenshotManager.ValidateTheTime(dt))
            {
                Console.WriteLine("Exit because the time retrieved is not as the time saved");
                succesful = false;
                return succesful;
            }

            //Step 15: Commit again the changes
            if (!_screenshotManager.CommitTheChange())
            {
                Console.WriteLine("Exit at second commiting the change");
                succesful = false;
                return succesful;
            }

            //Step 21: check if Prebuffering is no longer there
            if (!_screenshotManager.CheckIfOnDemandIsOn())
            {
                Console.WriteLine("Exit because Realtime data/OnDemand is not there, most likely is Prebuffering");
                succesful = false;
                return succesful;
            }

            //Step 22: Wait to see if the seconds are changing 
            if (succesful && !_screenshotManager.WaitForTicksToStart())
            {
                succesful = false;
                return succesful;
            }

            return succesful;
        }
    }
}


