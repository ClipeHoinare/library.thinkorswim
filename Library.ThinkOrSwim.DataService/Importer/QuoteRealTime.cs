﻿using Library.ThinkOrSwim.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.ThinkOrSwim.DataService.Importer
{
    public class QuoteRealTime
    {
        public DateTime StartDateTime { get; protected set; }
        public DateTime EndDateTime { get; protected set; }
        public string Symbol { get; protected set; }
        public string SecType { get; protected set; }
        public double Ask { get; set; }
        public double Bid { get; set; }
        public double Close { get; set; }
        public double Delta { get; set; }
        public double Gamma { get; set; }
        public double High { get; set; }
        public double Impl_vol { get; set; }
        public double Last { get; set; }
        public double Low { get; set; }
        public double Mark { get; set; }
        public double Open { get; set; }
        public double Strike { get; set; }
        public double Theta { get; set; }
        public double Vega { get; set; }

        public QuoteRealTime(string symbol, string secType)
        {
            this.Symbol = symbol;
            this.SecType = secType;
            StartDateTime = DateTime.Now;
        }

        public void SetValue(Quote quote)
        {
            if(quote.Updated)
            {
                switch(quote.DataType)
                {
                    case "Ask":
                        this.Ask = quote.Value;
                        break;
                    case "Bid":
                        this.Bid = quote.Value;
                        break;
                    case "Close":
                        this.Close = quote.Value;
                        break;
                    case "Delta":
                        this.Delta = quote.Value;
                        break;
                    case "Gamma":
                        this.Gamma = quote.Value;
                        break;
                    case "High":
                        this.High = quote.Value;
                        break;
                    case "Impl_vol":
                        this.Impl_vol = quote.Value;
                        break;
                    case "Last":
                        this.Last = quote.Value;
                        break;
                    case "Low":
                        this.Low = quote.Value;
                        break;
                    case "Mark":
                        this.Mark = quote.Value;
                        break;
                    case "Open":
                        this.Open = quote.Value;
                        break;
                    case "Strike":
                        this.Strike = quote.Value;
                        break;
                    case "Theta":
                        this.Theta = quote.Value;
                        break;
                    case "Vega":
                        this.Vega = quote.Value;
                        break;
                }
            }
        }

        public void SetEnDateTime()
        {
            EndDateTime = DateTime.Now;
        }
    }
}
