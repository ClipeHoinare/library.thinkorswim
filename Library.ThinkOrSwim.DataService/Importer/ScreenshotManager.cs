﻿using Library.Core.Repository.Screenshot;
using System;
using System.Collections.Generic;
using System.Drawing;
using Library.Domain.Base.Extensions;
using System.Threading.Tasks;

namespace Library.ThinkOrSwim.DataService.Importer
{
    public class ScreenshotManager
    {
        string _pathToImages = "C:\\projects\\sikuli\\ToSOnDemand\\";
        ScreenshotHandler _screenshotHandler = new ScreenshotHandler();
        User32Wrapper _user32Wrapper = new User32Wrapper("thinkorswim");
        Bitmap _realtimedataOnBtmp = null;
        Bitmap _stepbBtmp = null;
        Bitmap _stepfBtmp = null;
        Rectangle _stepbRectangle = Rectangle.Empty;
        Rectangle _stepfRectangle = Rectangle.Empty;
        Bitmap _monthDayBitmap = null;
        Rectangle _realtimedataRectangle = Rectangle.Empty;
        Rectangle _monthDayRectangle = Rectangle.Empty;
        Rectangle _datetimeRectangle = Rectangle.Empty;
        Rectangle _monitorRectangle = Rectangle.Empty;

        public int WaitTimeout { get; set; }

        public ScreenshotManager()
        {
            WaitTimeout = 45;
        }

        public Rectangle SetLocationAndSize()
        {

            var f = Rectangle.FromLTRB(left: 0, top: 0, right: 1160, bottom: 60);

            _user32Wrapper.SetLocationAndSize(f);

            return f;
        }
        public bool CheckIfOnDemandIsOn()
        {
            bool succesful = true;

            if (_realtimedataOnBtmp == null)
                _realtimedataOnBtmp = (Bitmap)Bitmap.FromFile(_pathToImages + "realtimedataon.png");

            if (_realtimedataRectangle == Rectangle.Empty)
            {
                _realtimedataRectangle = new Rectangle(new Point(3, 9), _realtimedataOnBtmp.Size);
            }

            if (!WaitForBitmapToRestore(_realtimedataRectangle, _realtimedataOnBtmp))
            {
                succesful = false;
            }

            if(succesful)
                succesful = CheckContainmentOnScreen(_stepbRectangle, _stepbBtmp);

            if (succesful)
                succesful = CheckContainmentOnScreen(_stepfRectangle, _stepfBtmp);

            if (_monitorRectangle== Rectangle.Empty)
                _monitorRectangle = _user32Wrapper.GetScreenSize();

            return succesful;
        }

        internal bool ValidateTheMonth(DateTime dt)
        {
            var monthString = dt.ToString("MMM") + ".png";
            var yearString = "Year"+  dt.ToString("yyyy")+ ".png";

            //var yearRectangle = new Rectangle(new Point(947, 70), new Size(26, 10));
            //var yBitScr = _screenshotHandler.GetScreenshot(yearRectangle, "pic\\" + yearString);
            //var monthRectangle = new Rectangle(new Point(837, 62), new Size(200, 26));
            //var mBitScr = _screenshotHandler.GetScreenshot(monthRectangle, "pic\\" + monthString);

            var mmmyyyyRectangle = new Rectangle(new Point(837, 62), new Size(200, 26));
            var mmmBtmp = (Bitmap)Bitmap.FromFile(_pathToImages + monthString);
            var yyyyBtmp = (Bitmap)Bitmap.FromFile(_pathToImages + yearString);

            var succesfulMmm = CheckContainmentOnScreen(mmmyyyyRectangle, mmmBtmp);
            var succesfulYyyy = CheckContainmentOnScreen(mmmyyyyRectangle, yyyyBtmp);

            var succesful = false;

            if (succesfulMmm && succesfulYyyy)
            {
                succesful = true;
            }

            return succesful;
        }

        protected bool WaitForBitmapToRestore(Rectangle rectangle, Bitmap bitmap, int timeout = 0)
        {
            bool started = false;

            if (timeout == 0)
                timeout = WaitTimeout * 2;

            var endDateTime = DateTime.Now.AddSeconds(timeout);
            while (endDateTime > DateTime.Now)
            {
                var task = CheckIsScreenshotSame(rectangle, bitmap);

                if (task.Result)
                {
                    started = true;
                    break;
                }
            }

            return started;
        }
        public async Task<bool> CheckIsScreenshotSame(Rectangle rectangle, Bitmap bitmap)
        {
            //https://stackoverflow.com/questions/23431595/task-yield-real-usages
            //wait for the system to process the changes
            var found = false;
            await Task.Delay(400);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            await Task.Yield();
            await Task.Delay(400);

            found = CheckExistenceOnScreen(rectangle, bitmap);

            return found;
        }
        private bool CheckExistenceOnScreen(Rectangle rectangle, Bitmap bitmap)
        {
            bool found = false;
            var secondBitmap = _screenshotHandler.GetScreenshot(rectangle);

            if (bitmap.CompareBitmapsFast(secondBitmap))
                found = true;
            return found;
        }
        private bool CheckContainmentOnScreen(Rectangle rectangle, Bitmap bitmap)
        {
            bool found = false;
            var secondBitmap = _screenshotHandler.GetScreenshot(rectangle);

            if (secondBitmap.FindBitmap(bitmap, out Point location))
                found = true;
            return found;
        }
        internal bool ValidateTheTime(DateTime dt)
        {
            var sametime = false;

            sametime = ClickOnAndSelectTime();

            if (sametime)
            {
                sametime = false;
                var time = dt.ToString("HH:mm:ss").Substring(0, 4);

                var timeText = _screenshotHandler.GetSelectAllTextToAndFromClipboard();

                if (!string.IsNullOrWhiteSpace(timeText) && timeText.Substring(0, 4).Equals(time))
                {
                    sametime = true;
                }
            }

            return sametime;
        }

        internal bool ClickOnCalendar()
        {
            //var calRectangle = new Rectangle(new Point(891, 34), new Size(14,14));
            //var dayBitScr = _screenshotHandler.GetScreenshot(calRectangle, "calendar.png");

            var calRectangle = new Rectangle(new Point(860, 31), new Size(72, 21));
            var calBtmp = (Bitmap)Bitmap.FromFile(_pathToImages + "calendar.png");

            var succesful = CheckContainmentOnScreen(calRectangle, calBtmp);

            if (succesful)
            {
                var calendarPoint = new Point(
                    (int)((_stepbRectangle.X + 2.5 * _stepbRectangle.Width) * 65535 / _monitorRectangle.Width),
                    (int)((_stepbRectangle.Y + 0.5 * _stepbRectangle.Height) * 65535 / _monitorRectangle.Height));

                _screenshotHandler.ClickAt(calendarPoint);
            }

            return succesful;
        }
        internal bool ClickOnAndSelectTime()
        {
            var timePoint= new Point(920, 265);
            var _timesBtm = (Bitmap)Bitmap.FromFile(_pathToImages + "times.png");
            var _rectangleTime = new Rectangle(new Point(830, 250), new Size(70, 30));

            var succesful = CheckContainmentOnScreen(_rectangleTime, _timesBtm);

            if (succesful)
            {
                var calendarPoint = new Point(
                    (int)((timePoint.X) * 65535 / _monitorRectangle.Width),
                    (int)((timePoint.Y) * 65535 / _monitorRectangle.Height));

                _screenshotHandler.ClickAndSelectAt(calendarPoint);
            }

            return succesful;
        }

        internal void TypeTimeValue(string text)
        {
            _screenshotHandler.TypeTimeValue(text);
        }
        internal bool SelectDay(DateTime dt, string flag)
        {
            bool identified = true;
            var dayString = dt.Day.ToString();

            var dayOfWeek = dt.GetDayOfWeek();
            var weekOfMonth = dt.GetWeekOfMonth();
            var weeksInMonth = dt.GetWeeksInMonth();

            var filename = "W" + weeksInMonth.ToString() + flag + dayString + ".png";

            var dayBitHD = (Bitmap)Bitmap.FromFile(_pathToImages + filename);

            var dayRectangle =  GetDayRectangle(dt);

            if (!identified && identified)
            {
                // keep it for saving the mark file
                var rect = new Rectangle(dayRectangle.X + 2, dayRectangle.Y + 5, dayRectangle.Width - 3 - 7, dayRectangle.Height - 5 - 5);
                var dayBitScr = _screenshotHandler.GetScreenshot(rect, "pic\\" + filename);
            }

            if (CheckContainmentOnScreen(dayRectangle, dayBitHD))
            {
                var dayMidPoint = new Point(
                    (dayRectangle.X + dayRectangle.Width / 2) * 65535 / _monitorRectangle.Width
                    , (dayRectangle.Y + dayRectangle.Height / 2) * 65535 / _monitorRectangle.Height);

                _screenshotHandler.ClickAt(dayMidPoint);
            }
            else
            {
                identified = false;
            }

            return identified;
        }
        internal bool CheckSelectedDay(DateTime dt, string flag)
        {
            bool identified = true;
            var dayString = dt.Day.ToString();

            var dayOfWeek = dt.GetDayOfWeek();
            var weekOfMonth = dt.GetWeekOfMonth();
            var weeksInMonth = dt.GetWeeksInMonth();

            var filename = "W" + weeksInMonth.ToString() + flag + dayString + ".png";

            var dayBitHD = (Bitmap)Bitmap.FromFile(_pathToImages + filename);

            var dayRectangle = GetDayRectangle(dt);

            if (!identified && identified)
            {
                // keep it for saving the mark file
                var rect = new Rectangle(dayRectangle.X + 2, dayRectangle.Y + 5, dayRectangle.Width - 3 - 7, dayRectangle.Height - 5 - 5);
                var dayBitScr = _screenshotHandler.GetScreenshot(rect, "pic\\" + filename);
            }

            if (!CheckContainmentOnScreen(dayRectangle, dayBitHD))
            {
                identified = false;
            }

            return identified;
        }
        private Rectangle GetDayRectangle(DateTime dt)
        {
            //: identify how many row weeks on the month, identify the position of the day in the month row/col, ...
            var dayOfWeek = dt.GetDayOfWeek();
            var weekOfMonth = dt.GetWeekOfMonth();
            var weeksInMonth = dt.GetWeeksInMonth();

            var daySize = weeksInMonth == 5 ? new Size(26, 22) : weeksInMonth == 6 ? new Size(26, 22) : new Size(26, 22);
            var xDelta = weeksInMonth == 5 ? 28 : weeksInMonth == 6 ? 28 : 28;
            var yDelta = weeksInMonth == 5 ? 24 : weeksInMonth == 6 ? 24 : 24;

            var dayRectangle = new Rectangle(new Point(845 + xDelta * dayOfWeek, 96 + yDelta * weekOfMonth), daySize);

            return dayRectangle;
        }
        public void SetNewDatetime()
        {
            //var ala = _screenshotHandler.GetScreenshot(new Rectangle(new Point(861,34), new Size(18,13)),"stepb.png");

            if (_stepbBtmp == null)
            {
                _stepbBtmp = (Bitmap)Bitmap.FromFile(_pathToImages + "stepb.png");
                _stepbRectangle = new Rectangle(new Point(845, 31), new Size(36,21));
            }

            if (_stepfBtmp == null)
            {
                _stepfBtmp = (Bitmap)Bitmap.FromFile(_pathToImages + "stepf.png");
                _stepfRectangle = new Rectangle(new Point(998, 31), _stepfBtmp.Size);
            }

            if (_monthDayRectangle == Rectangle.Empty)
            {
                _monthDayRectangle = new Rectangle(new Point(_stepbRectangle.X + _stepbRectangle.Width, _stepbRectangle.Y), new Size(2 * _stepbRectangle.Width, _stepbRectangle.Height));
                _datetimeRectangle = new Rectangle(new Point(_stepfRectangle.X - 2 * _stepbRectangle.Width, _stepfRectangle.Y), new Size(2 * _stepfRectangle.Width, _stepfRectangle.Height));
            }

            //Step 2: get a screenshot of the month and the day
            _monthDayBitmap = _screenshotHandler.GetScreenshot(_monthDayRectangle);
        }

        internal bool CommitTheChange()
        {
            var goRectangle = new Rectangle(new Point(994, 251), new Size(44, 29));
            var goBitHD = (Bitmap)Bitmap.FromFile(_pathToImages + "go.png");

            var succesful = CheckContainmentOnScreen(goRectangle, goBitHD);

            if (succesful)
            {
                var goMidPoint = new Point(
                    (goRectangle.X + goRectangle.Width / 2) * 65535 / _monitorRectangle.Width
                    , (goRectangle.Y + goRectangle.Height / 2) * 65535 / _monitorRectangle.Height);

                _screenshotHandler.ClickAt(goMidPoint);
            }

            return succesful;
        }

        protected bool WaitForOnDemandOn(Rectangle rectangle, Bitmap bitmap, bool checkDate)
        {
            //Check to see if the calendar date changed
            bool changed = true;

            changed = WaitForBitmapChange(rectangle, bitmap, WaitTimeout);

            return changed;
        }
        public bool WaitForDateToChange(bool checkDate)
        {
            //Check to see if the calendar date changed
            bool changed = true;

            if (checkDate)
            {
                changed = WaitForBitmapChange(_monthDayRectangle, _monthDayBitmap, WaitTimeout);
            }

            return changed;
        }
        public bool WaitForTicksToStart()
        {
            //get a screenshot of the screen for the datetime 
            //  and then take it as many time as needed until the screenshot(time/seconds) changed

            bool started = false;
            var initialTimeBitmap = _screenshotHandler.GetScreenshot(_datetimeRectangle);

            started = WaitForBitmapChange(_datetimeRectangle, initialTimeBitmap);

            return started;
        }
        protected bool WaitForBitmapChange(Rectangle rectangle, Bitmap bitmap, int timeout = 0)
        {
            bool started = false;

            if (timeout == 0)
                timeout = WaitTimeout;

            var endDateTime = DateTime.Now.AddSeconds(timeout);
            while (endDateTime > DateTime.Now)
            {
                var task = CheckIsScreenshotSame(rectangle, bitmap);

                if (!task.Result)
                {
                    started = true;
                    break;
                }
            }

            return started;
        }
        //public async Task<bool> CheckIsScreenshotSame(Rectangle rectangle, Bitmap bitmap)
        //{
        //    //https://stackoverflow.com/questions/23431595/task-yield-real-usages
        //    //wait for the system to process the changes
        //    var found = false;
        //    await Task.Delay(400);
        //    GC.Collect();
        //    GC.WaitForPendingFinalizers();
        //    await Task.Yield();
        //    await Task.Delay(400);

        //    var secondBitmap = _screenshotHandler.GetScreenshot(rectangle);

        //    if (_screenshotHandler.CompareBitmapsFast(bitmap, secondBitmap))
        //        found = true;

        //    return found;
        //}



        //private bool SikuliGetRealtimeRectangle()
        //{
        //    bool succesful = true;
        //    if (_realtimedataRectangle == Rectangle.Empty)
        //    {
        //        try
        //        {
        //            var realtimedataRegion = _session.Find(new SikuliSharp.FilePattern(_pathToImages + "realtimedata.png", 0.5f));
        //            _realtimedataRectangle = new Rectangle(realtimedataRegion.GetX(), realtimedataRegion.GetY(), realtimedataRegion.GetW(), realtimedataRegion.GetH());
        //        }
        //        catch (Exception ex)
        //        {
        //            succesful = false;
        //        }
        //    }
        //    return succesful;
        //}

    }
}
