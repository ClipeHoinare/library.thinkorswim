﻿using Library.Domain.Infrastructure;
using Library.Domain.Jackdow;
using Library.Domain.Trade.Base;
using Library.Domain.Trade.TOS;
using Library.Domain.Trade.Jackdow;
using Library.ThinkOrSwim.Adapter;
using Library.Wrapper.Trady.Yahoo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.ThinkOrSwim.DataService.Importer
{
    public class TosMarketImporter : IImporterInter<QuoteRealTimeItem, FinanceParameter>
    {
        private static readonly DateTime UnixMinDateTime = new DateTime(1901, 12, 13);
        private static readonly DateTime UnixMaxDateTime = new DateTime(2038, 1, 19);

        private TosMarketController _controller;

        public TosMarketController Controller { get => _controller; set => _controller = value; }

        public TosMarketImporter() : this(new TosMarketController(30))
        {
        }
        public TosMarketImporter(TosMarketController controller)
        {
            _controller = controller;

            _controller.StockReceived += OnStockReceived;
            _controller.QuoteReceived += OnQuoteReceived;
            _controller.NoiseReceived += OnNoiseReceived;

            //if (parameter.Timeout != 0)
            //    _controller.Timeout = parameter.Timeout;
        }
        /// <summary>
        /// Should do: Topics should be mapped to a Trady common domain
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="endDateTime"></param>
        /// <param name="timeoutSec"></param>
        /// <returns></returns>
        public Tuple<IReadOnlyList<QuoteRealTimeItem>, Exception> ImportSync(FinanceParameter parameter)
        {
            var quotes = new List<QuoteRealTimeItem>();
            Exception retEx = null;
            try
            {
                _controller.ActivateClient();

                if(parameter.Timeout != 0 )
                _controller.Timeout = parameter.Timeout;

                _controller.AddStockRequest(parameter.Symbol, parameter.SecId, parameter.SecType, parameter.StartDateTime);

                var topics = _controller.PullData();

                var candles = new List<Candle>() { null };

                topics.TryRemove(parameter.Symbol, out var q);

                quotes.Add(new QuoteRealTimeItem(q.Symbol, parameter.SecId, q.SecType) { Ask=q.Ask, Bid=q.Bid, Close = q.Close, Open = q.Open, High = q.High, Low=q.Low, Last = q.Last, Mark=q.Mark, StartDateTime = q.StartDateTime, EndDateTime=q.EndDateTime });
            }
            catch (Exception ex)
            {
                retEx = ex;
            }

            return new Tuple<IReadOnlyList<QuoteRealTimeItem>, Exception>(quotes, retEx);
        }

        public Tuple<QuoteRealTimeItem, Exception> GetOneToProcess()
        {
            QuoteRealTimeItem quote = null;
            Exception retEx = null;
            try
            {
                var q = _controller.PullOneData();
                if (q != null && !string.IsNullOrWhiteSpace(q.Symbol))
                {
                    var h = _controller.Topics.QuoteHeaders[q.Symbol];
                    quote = new QuoteRealTimeItem(q.Symbol, h.SecId, q.SecType) { Ask = q.Ask, Bid = q.Bid, Close = q.Close, Open = q.Open, High = q.High, Low = q.Low, Last = q.Last, Mark = q.Mark, StartDateTime = q.StartDateTime, EndDateTime = q.EndDateTime, TradingDateTime = h.TradingDateTime };
                }
            }
            catch (Exception ex)
            {
                retEx = ex;
            }

            return new Tuple<QuoteRealTimeItem, Exception>(quote, retEx);
        }
        public Tuple<IReadOnlyList<QuoteRealTimeItem>, Exception> GetDataLeft()
        {
            var quotes = new List<QuoteRealTimeItem>();
            Exception retEx = null;
            try
            {
                var keys = _controller.GetDataLeft().QuoteRealTimers.Keys.ToArray();
                for(int i = 0; i < keys.Length; i++)
                {
                    var symbol = keys[i];
                    var q = _controller.PullOneQuote(symbol);
                    var h = _controller.GetOneHeader(symbol);

                    quotes.Add(new QuoteRealTimeItem(q.Symbol, h.SecId, q.SecType) { Ask = q.Ask, Bid = q.Bid, Close = q.Close, Open = q.Open, High = q.High, Low = q.Low, Last = q.Last, Mark = q.Mark, StartDateTime = q.StartDateTime, EndDateTime = q.EndDateTime });
                }
            }
            catch (Exception ex)
            {
                retEx = ex;
            }

            return new Tuple<IReadOnlyList<QuoteRealTimeItem>, Exception>(quotes, retEx);
        }
        public bool AddParameter(FinanceParameter parameter)
        {
            var quotes = true;
            Exception retEx = null;
            try
            {
                _controller.ActivateClient();

                _controller.AddStockRequest(parameter.Symbol, parameter.SecId, parameter.SecType, parameter.StartDateTime);
            }
            catch (Exception ex)
            {
                retEx = ex;
                quotes = false;
            }

            return quotes;
        }
        private void OnStockReceived(TosMarketController sender, Quote quote, EventArgs e)
        {
            Console.WriteLine("{0} completed", quote.Symbol);
        }
        private void OnQuoteReceived(TosMarketController sender, Quote quote, EventArgs e)
        {
            Console.WriteLine("{0} {1} {2}: ${3}", quote.Symbol, quote.DataType, quote.CounterId, quote.Value);
        }
        private void OnNoiseReceived(TosMarketController sender, Tuple<int, double> tuple, EventArgs e)
        {
            Console.WriteLine("Noise: {1} - {2}", tuple.Item1, tuple.Item2);
        }
    }
}
