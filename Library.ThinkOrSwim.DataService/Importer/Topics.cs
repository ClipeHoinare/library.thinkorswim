﻿using Library.ThinkOrSwim.Adapter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Library.ThinkOrSwim.DataService.Importer
{
    public class Topics
    {
        public ConcurrentDictionary<string, QuoteHeader> QuoteHeaders = new ConcurrentDictionary<string, QuoteHeader>();
        public ConcurrentDictionary<string, QuoteRealTime> QuoteRealTimers = new ConcurrentDictionary<string, QuoteRealTime>();
        public Topics()
        {
        }
        public void Add(Quote quote, int secId, DateTime dt, string stockType)
        {
            if (!this.QuoteRealTimers.ContainsKey(quote.Symbol))
            {
                this.QuoteRealTimers[quote.Symbol] = new QuoteRealTime(quote.Symbol, stockType);
                this.QuoteHeaders[quote.Symbol] = new QuoteHeader(quote.Symbol, secId, stockType) { TradingDateTime = dt };
            }
            this.QuoteHeaders[quote.Symbol].CountSend += quote.CounterId;
        }
        public void Update(Quote quote)
        {
            if (this.QuoteRealTimers.ContainsKey(quote.Symbol))
                this.QuoteRealTimers[quote.Symbol].SetValue(quote);

            //use binary to be sure you didn't received more than one fire back on the same counterId, it will mess up the calculation
            if ((((byte)QuoteHeaders[quote.Symbol].CountReceived) & quote.CounterId) == 0)
                QuoteHeaders[quote.Symbol].CountReceived = QuoteHeaders[quote.Symbol].CountReceived + quote.CounterId;
        }
        public void TryRemove(string symbol, out QuoteRealTime quoteRealTime)
        {
            this.QuoteRealTimers.TryRemove(symbol, out quoteRealTime);
        }

        public void SymbolComplete(Quote quote)
        {
            this.QuoteHeaders[quote.Symbol].Completed = true;
            if (this.QuoteRealTimers.ContainsKey(quote.Symbol))
                this.QuoteRealTimers[quote.Symbol].SetEnDateTime();
        }
        public bool CheckIfSymbolCompleted(Quote quote)
        {
            return quote != null && quote.Symbol != null && this.QuoteHeaders[quote.Symbol].CountSend == this.QuoteHeaders[quote.Symbol].CountReceived && this.QuoteHeaders[quote.Symbol].Completed == false;
        }
    }
    public class QuoteHeader
    {
        public string Symbol { get; protected set; }
        public int SecId { get; protected set; }
        public DateTime TradingDateTime { get; set; }
        public string SecType { get; protected set; }
        public int CountSend { get; set; }
        public int CountReceived { get; set; }
        public bool Completed { get; set; }

        internal QuoteHeader(string symbol, int secId, string secType)
        {
            this.Symbol = symbol;
            this.SecId = secId;
            this.SecType = secType;
            this.CountSend = 0;
            this.CountReceived = 0;
            this.Completed = false;
        }
    }
}

