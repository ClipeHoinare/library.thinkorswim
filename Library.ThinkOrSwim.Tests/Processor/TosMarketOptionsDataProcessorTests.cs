﻿using Library.Core.Repository.Screenshot;
using Library.Repository.StorageArmory;
using Library.ThinkOrSwim.DataService.Importer;
using Library.ThinkOrSwim.DataService.Processor;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SikuliSharp;
using System;

namespace Library.ThinkOrSwim.DataService.Tests.Processor
{
    [TestClass]
    public class TosMarketOptionsDataProcessorTests
    {
        [TestMethod]
        public void TosStockGetData()
        {
            (new StorageSqlQueue()).Start();
            var c1 = new TosMarketStocksDataProcessor();
            
            c1.Process(null);


            (new StorageSqlQueue()).Stop();
        }
        [TestMethod]
        public void TosSikuli()
        {
            var tosController = new ToSSikuliController();

            tosController.SetToSDateAndTime(DateTime.Parse("2020-01-03 09:30:00"));
            tosController.SetToSDateAndTime(DateTime.Parse("2020-01-17 10:00:00"));
            tosController.SetToSDateAndTime(DateTime.Parse("2020-01-15 10:30:00"));
            tosController.SetToSDateAndTime(DateTime.Parse("2020-01-22 11:00:00"));

        }
        [TestMethod]
        public void TestScreenShoot()
        {
            ScreenshotHandler sh = new ScreenshotHandler();

            //sh.GetScreenshot();

        }

    }

    //http://automation-home.blogspot.com/2014/09/AutomateCalcAppWithJavaAndSikuli.html
    //https://subscription.packtpub.com/book/application_development/9781782167877/1/ch01lvl1sec06/top-13-features-you-need-to-know-about




}
